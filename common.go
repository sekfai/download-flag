package main

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strings"
	"time"
)

type Result struct {
	cc     string
	status string // one of "ok", "notFound", "error"
}

var pop20cc = []string{
	"CN", "IN", "US", "ID",
	"BR", "PK", "NG", "BD",
	"RU", "JP", "MX", "PH",
	"VN", "ET", "EG", "DE",
	"IR", "TR", "CD", "FR",
}

const defaultServer = "LOCAL"
const destDir = "downloads/"

var all bool
var every bool
var limit int
var numConcurReq int
var server string
var verbose bool

var servers = map[string]string{
	"LOCAL": "http://localhost:8001/flags",
	"DELAY": "http://localhost:8002/flags",
	"ERROR": "http://localhost:8003/flags",
	"GOLOCAL": "http://localhost:8080/flags",
}

var serverOptions []string

func init() {
	flag.BoolVar(&all, "all", false, "get all available flags (AD to ZW)")
	flag.BoolVar(&all, "a", false, "get all available flags (AD to ZW)")

	flag.BoolVar(&every, "every", false, "get flags for every possible code (AA..ZZ)")
	flag.BoolVar(&every, "e", false, "get flags for every possible code (AA..ZZ)")

	flag.IntVar(&limit, "limit", math.MaxInt32, "limit to N first codes")
	flag.IntVar(&limit, "l", math.MaxInt32, "limit to N first codes")

	flag.IntVar(&numConcurReq, "num_concur_req", defaultConcurReq, "number of concurrent requests")
	flag.IntVar(&numConcurReq, "n", defaultConcurReq, "number of concurrent requests")

	serverOptions := make([]string, len(servers))
	i := 0
	for k := range servers {
		serverOptions[i] = k
		i++
	}
	usage := "Server to hit; one of " + strings.Join(serverOptions, ", ")
	flag.StringVar(&server, "server", defaultServer, usage)
	flag.StringVar(&server, "s", defaultServer, usage)

	flag.BoolVar(&verbose, "verbose", false, "output detailed progress info")
	flag.BoolVar(&verbose, "v", false, "output detailed progress info")
}

func getFlag(baseUrl, cc string) (*http.Response, error) {
	cc = strings.ToLower(cc)
	u, err := url.Parse(baseUrl)
	if err != nil {
		log.Fatal(err)
	}
	u.Path = path.Join(u.Path, cc, cc+".gif")
	res, err := http.Get(u.String())
	return res, err
}

func saveFlag(res *http.Response, fileName string) {
	p := filepath.Join(destDir, fileName)
	f, err := os.Create(p)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	_, err = io.Copy(f, res.Body)
	if err != nil {
		log.Fatal(err)
	}
}

func downloadOne(cc, baseUrl string, verbose bool) Result {
	var status string
	res, err := getFlag(baseUrl, cc)
	defer res.Body.Close()

	if err != nil {
		status = "error"
		if verbose {
			log.Printf("%s %s\n", cc, err)
		}
		return Result{cc: cc, status: status}
	}

	switch res.StatusCode {
	case http.StatusOK:
		status = "ok"
		saveFlag(res, strings.ToLower(cc)+".gif")
	case http.StatusNotFound:
		status = "notFound"
	default:
		status = "error"
	}

	if verbose {
		log.Printf("%s %s\n", cc, res.Status)
	}
	return Result{cc: cc, status: status}
}

func genccList(every, all bool, ccArgs []string, limit int) []string {
	var codes []string

	if every {
		codes = everycc()
	} else if all {
		codes = allcc()
	} else {
		codes = expandccArgs(ccArgs)
	}

	if limit < len(codes) {
		return codes[:limit]
	}
	return codes
}

func everycc() []string {
	// not sure efficient or not due to many calls to type conversion rune->string
	codes := make([]string, 26*26)
	i := 0
	for j := 'A'; j <= 'Z'; j++ {
		for k := 'A'; k <= 'Z'; k++ {
			codes[i] = string(j) + string(k)
			i++
		}
	}
	return codes
}

/*
func everycc() []string {
	// another implementation, fewer calls to type conversion rune->string
	A_Z := make([]string, 26)
	i := 0
	for j := 'A'; j <= 'Z'; j++ {
		A_Z[i] = string(j)
		i++
	}

	codes := make([]string, 26*26)
	i = 0
	for _, s1 := range A_Z {
		for _, s2 := range A_Z {
			codes[i] = s1 + s2
			i++
		}
	}
	return codes
}
*/

func allcc() []string {
	file, err := os.Open("country_codes.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	b, err := ioutil.ReadAll(file)
	codes := strings.Fields(string(b))
	sort.Strings(codes)
	return codes
}

func expandccArgs(ccArgs []string) []string {
	A_Z := make([]string, 26)
	i := 0
	for j := 'A'; j <= 'Z'; j++ {
		A_Z[i] = string(j)
		i++
	}
	set := make(map[string]bool)
	for _, ca := range ccArgs {
		if len(ca) == 1 && IsLetter(ca) {
			ca = strings.ToUpper(ca)
			for _, s := range A_Z {
				set[ca+s] = true
			}
		} else if len(ca) == 2 && IsLetter(ca) {
			ca = strings.ToUpper(ca)
			set[ca] = true
		} else {
			fmt.Fprintf(os.Stderr, "ccArg %s: must be A to Z or AA to ZZ\n", ca)
			os.Exit(1)
		}
	}
	codes := make([]string, len(set))

	i = 0
	for k := range set {
		codes[i] = k
		i++
	}
	sort.Strings(codes)
	return codes
}

func IsLetter(s string) bool {
	for _, r := range s {
		if (r < 'a' || r > 'z') && (r < 'A' || r > 'Z') {
			return false
		}
	}
	return true
}

func min(value int, values ...int) int {
	for _, v := range values {
		if v < value {
			value = v
		}
	}
	return value
}

func initialReport(ccList []string, numWorkers int, server string) {
	var ccMsg string
	var plural string
	fmt.Println(server, "site:", servers[server])

	if len(ccList) <= 10 {
		ccMsg = strings.Join(ccList, ", ")
	} else {
		ccMsg = fmt.Sprintf("from %s to %s", ccList[0], ccList[len(ccList)-1])
	}
	if len(ccList) != 1 {
		plural = "s"
	} else {
		plural = ""
	}
	fmt.Printf("Searching for %d flag%s: %s\n", len(ccList), plural, ccMsg)

	if numWorkers != 1 {
		plural = "s"
	} else {
		plural = ""
	}
	fmt.Printf("%d concurrent connection%s will be used.\n", numWorkers, plural)
}

func finalReport(counter map[string]int, elapsed time.Duration) {
	var n int
	var plural string
	fmt.Println(strings.Repeat("-", 20))

	if n = counter["ok"]; n != 1 {
		plural = "s"
	} else {
		plural = ""
	}
	fmt.Printf("%d flag%s downloaded.\n", n, plural)

	if n = counter["notFound"]; n > 0 {
		fmt.Println(n, "not found.")
	}

	if n = counter["error"]; n != 1 {
		plural = "s"
	} else {
		plural = ""
	}
	fmt.Printf("%d error%s.\n", n, plural)

	fmt.Println("Elapsed time:", elapsed)
}
