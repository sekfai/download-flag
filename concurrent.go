package main

import (
	"flag"
	"fmt"
	"os"
	"sort"
	"strings"
	"time"
)

const defaultConcurReq = 4
const maxConcurReq = 1000

func main() {
	flag.Parse()

	if limit < 1 {
		fmt.Fprintf(os.Stderr, "Usage error: -l/-limit %d: must be >= 1\n", limit)
		os.Exit(1)
	}

	if numConcurReq < 1 {
		fmt.Fprintf(os.Stderr, "Usage error: -n/-num_concur_req %d: must be >= 1\n", numConcurReq)
		os.Exit(1)
	}

	baseUrl, ok := servers[strings.ToUpper(server)]
	if !ok {
		fmt.Fprintf(os.Stderr, "Usage error: -s/-server %s: must be one of %s\n", server, strings.Join(serverOptions, ", "))
		os.Exit(1)
	}

	ccList := genccList(every, all, flag.Args(), limit)
	if len(ccList) == 0 {
		ccList = pop20cc
		sort.Strings(ccList)
	}
	numWorkers := min(numConcurReq, maxConcurReq, len(ccList))
	initialReport(ccList, numWorkers, strings.ToUpper(server))

	start := time.Now()
	counter := downloadMany(ccList, baseUrl, numWorkers)
	elapsed := time.Since(start)

	finalReport(counter, elapsed)
}

func downloadMany(ccList []string, baseUrl string, numWorkers int) map[string]int {
	counter := make(map[string]int)
	jobs := make(chan string)
	results := make(chan Result)

	// workers receiving jobs
	for i := 0; i < numWorkers; i++ {
		go func(id int) {
			for cc := range jobs {
				if verbose {
					fmt.Printf("worker #%d received %s\n", id, cc)
				}
				results <- downloadOne(cc, baseUrl, verbose)
			}
		}(i)
	}

	// sending jobs to workers
	go func() {
		for _, cc := range ccList {
			jobs <- cc
		}
		close(jobs)
	}()

	// waiting for results to come back
	for i := 0; i < len(ccList); i++ {
		r := <-results
		counter[r.status] += 1
	}

	return counter
}

/*
// one worker/goroutine per flag implemention
func downloadMany(ccList []string, baseUrl string, numWorkers int) map[string]int {
	counter := make(map[string]int)
	results := make(chan Result)

	// assigning each job to a new worker
	for i, c := range ccList {
		go func(id int, cc string) {
			if verbose {
				fmt.Printf("worker #%d received %s\n", id, cc)
			}
			results <- downloadOne(cc, baseUrl, verbose)
		}(i, c)
	}

	// waiting for results to come back
	for i := 0; i < len(ccList); i++ {
		r := <-results
		counter[r.status] += 1
	}

	return counter
}
*/
