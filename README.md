$ go run common.go concurrent.go -s local -n 3 -l 11 -v p

LOCAL site: http://localhost:8001/flags  
Searching for 11 flags: from PA to PK  
3 concurrent connections will be used.  
worker #2 received PC  
worker #1 received PB  
worker #0 received PA  
2020/07/30 17:49:57 PC 404 Not Found  
worker #2 received PD  
2020/07/30 17:49:57 PB 404 Not Found  
worker #1 received PE  
2020/07/30 17:49:57 PD 404 Not Found  
worker #2 received PF  
2020/07/30 17:49:57 PA 200 OK  
worker #0 received PG  
2020/07/30 17:49:57 PF 404 Not Found  
2020/07/30 17:49:57 PE 200 OK  
worker #1 received PH  
worker #2 received PI  
2020/07/30 17:49:57 PG 200 OK  
worker #0 received PJ  
2020/07/30 17:49:57 PH 200 OK  
worker #1 received PK  
2020/07/30 17:49:57 PI 404 Not Found  
2020/07/30 17:49:57 PJ 404 Not Found  
2020/07/30 17:49:57 PK 200 OK  
--------------------  
5 flags downloaded.  
6 not found.  
0 errors.  
Elapsed time: 3.410378ms
